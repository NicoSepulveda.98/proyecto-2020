**VISUALIZACION DE UNA PROTEINA**


Este proyecto consiste en la manipulación de proteínas mediante un visualizador que consiste en entregar al usuario la posibilidad de subir cualquier archivo tipo .pdb. Opción de descarga:
http://www.rcsb.org/search?request=%7B%22query%22%3A%7B%22type%22%3A%22terminal%22%2C%22service%22%3A%22text%22%2C%22node_id%22%3A0%7D%2C%22return_type%22%3A%22entry%22%2C%22request_options%22%3A%7B%22pager%22%3A%7B%22start%22%3A0%2C%22rows%22%3A100%7D%2C%22scoring_strategy%22%3A%22combined%22%2C%22sort%22%3A%5B%7B%22sort_by%22%3A%22rcsb_accession_info.initial_release_date%22%2C%22direction%22%3A%22desc%22%7D%5D%7D%2C%22request_info%22%3A%7B%22src%22%3A%22ui%22%2C%22query_id%22%3A%22690ad8320d15fa2f01c3406df613189b%22%7D%7D

Al cargar el archivo al programa se mostrara datos, como por ejemplo: Titulo, autor, detalles principales, etc. Además entrega la imagen de la proteína que se ejecuto. 

Da la posiblidad de que el usuario escoga entre cuatro opciones de muestra: 
- Atom
- Hetatm
- Anisou
- Others

Al mostrar en pantalla el contenido de cada modulo de entrega en un espacio al inferior de la ventana principal que al presionar un existente boton de guardado, permite guardar de forma automatica su selección.
Podra ver la información en el boton de 'Informacion'. 



**Construido con**

Este proyecto de programación fue construido bajo el lenguaje de programación multiparadigma Python, las librerías biopandas, os, pymol y GObject. Además del programa Glade.



**Pre-requisitos**

Para ejecutar el código debe tener instalada la versión python 3. Si no lo tiene instalado a continuación le dejamos los pasos a seguir para instalarlo.

Si está usando Ubuntu, puedes instalar python 3 con los siguientes comandos:

`sudo apt-get install python3`

Si está usando debian 8 o anteriores, puedes instalar python con el siguiente comando (es necesario ingresar como superusuario ante de proceder a escribir el comando):

`apt-get install python3`

Si está usando debian 10, puedes instalar python con el siguiente comando:

`apt install python3`

Si ocupa otro sistema operativo como Windows, puede optar por instalar python idle versión 3.8 u otro entorno de desarrollo integrado para Python.

Además como para crear este proyecto se hizo uso de la librería pandas, la cual también debe ser instalada, a continuación te guiamos como hacerlo:



**Instalación pip3**


`sudo apt install pip3`



**Instalación de biopandas.**



Para Ubuntu:


`sudo pip3 install biopandas`


Para debian:

Es necesario ingresar como superusuario antes de ejecutar el comando.


`pip3 install biopandas`


Para Windows:


`install biopandas`



**Instalación pymol**

`su apt-get install python3-pymol`
`su apt-get install pymol`



**Instalación de GObject**


`sudo pip3 install PyGObject`
`sudo apt install python3-gi`



**Instalación de Glade**


`sudo apt install glade`



**Instalación**

Debe descargar el código el cual se encuentra alojado en GitLab.
Una vez hecho esto, donde se guardó el archivo, debe abrir una terminal y escribir en ella el siguiente comando:

`python3 ProyectoProt.py`



**Autores**

**Nicolás Sepúlveda** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.

**Simone Urrutia** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.
