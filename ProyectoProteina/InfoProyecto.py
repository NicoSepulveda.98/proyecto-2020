import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MainInformacion():

    def __init__(self, option = None):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/MainWindow.ui")
        self.window = self.builder.get_object('window_inf')
        self.boton = self.builder.get_object('btn_cerrar_inf')
        self.boton.set_label('Aceptar')
        self.window.show_all()


