# !/usr/bin/env python
# -*- coding : utf -8 -* -

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from filechooser import MainChooser
from InfoProyecto import MainInformacion
from biopandas.pdb import PandasPdb
import os
import __main__
__main__.pymol_argv = ['pymol', '-qc']
import pymol
pymol.finish_launching()

class MainWindow():

    def __init__(self):
        # CONSTRUCTOR
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/MainWindow.ui")
        self.window = self.builder.get_object('main_window')
        self.window.connect ('destroy', Gtk.main_quit)
        self.window.set_title('VISUALIZADOR DE PROTEINAS')
        self.window.set_default_size(800, 600)
        # CREANDO BOTONES
        load_btn = self.builder.get_object('btn_abrir')
        load_btn.set_label('Abrir')
        save_as_btn = self.builder.get_object('btn_guardar')
        save_as_btn.set_label('Guardar')
        load_alumnos = self.builder.get_object('btn_alumnos')
        load_alumnos.set_label('Informacion')
        # CREANDO FUNCION DE BOTONES
        load_alumnos.connect('clicked', self.on_clicked_info)
        load_btn.connect('clicked', self.open_filechooser, 'open')
        save_as_btn.connect('clicked', self.open_filechooser, 'save')
        # LABEL ENTRADA 1000 CARACTERES
        self.caracteres = self.builder.get_object('infor_entrada')
        # LABEL AUTOR
        self.autor = self.builder.get_object('autor_name')
        # NOMBRE PROTEINA EN LABEL
        self.nombre_proteina = self.builder.get_object('nombre_proteina')
        # ENTREGA INFORMACION SELECCIONADA
        self.seleccion = self.builder.get_object('informacion_selec')
        # PROTEINA COMPLETA LISTSTORE
        self.proteina_completa = self.builder.get_object('proteina_completa')
        # IMAGEN GENERADA
        self.imagen = self.builder.get_object('image_prote')
        # COMBOBOX_TEXT
        self.comboboxtext = self.builder.get_object('combobox_text')
        self.modelo = Gtk.ListStore(str)
        # COMBOBOX CHANGED
        self.comboboxtext.connect('changed', self.combobox_text_cambio)
        renderer_text = Gtk.CellRendererText()
        lista = ['ATOM', 'HETATM', 'ANISOU', 'OTHERS']
        self.comboboxtext.pack_start(renderer_text, True)

        for i in lista:
            self.modelo.append([i])
        self.comboboxtext.set_model(self.modelo)
        # ABRIR VENTANA
        self.window.show_all()

    # ENTRADA A INFORMACION
    def on_clicked_info(self, btn = None):
        self.salida = MainInformacion()
        salir = self.salida.window.run()

        if salir == Gtk.ResponseType.ACCEPT:
            self.salida.window.destroy()

    # INICIADOR DEL IMPORTCHOOSER PARA LA VENTANA DE BUSQUEDA
    def open_filechooser(self, btn = None, opt = None):
        filechooser = MainChooser(option = opt)
        dialogo = filechooser.dialogo

        # FILTRAR LOS ARCHIVOS A BUSCAR A PDB
        if opt == 'open':
            filter_pdb = Gtk.FileFilter()
            filter_pdb.set_name('PDB Files')
            filter_pdb.add_mime_type('application/x-aportisdoc')
            dialogo.add_filter(filter_pdb)

        response = dialogo.run()

        if response == Gtk.ResponseType.OK:
            self.cargar(dialogo.get_filename())
            self.new = dialogo.get_filename()
            self.reconocer_nombre()

        elif response == Gtk.ResponseType.ACCEPT:
            self.guardar_archivo(dialogo.get_filename())
        dialogo.destroy()

    # RECONOCER NOMBRE DEL ARCHIVO
    def reconocer_nombre(self):
        self.reconocer = os.path.split(self.new)
        self.nombre = str(self.reconocer[1])
        self.nombre_proteina.set_text(self.nombre)
        self.imagen_proteina()

    # GUARDAR ARCHIVO
    def guardar_archivo(self, x):
        if self.name == 'ATOM':
            with open(x, 'w') as newfile:
                self.carga.to_pdb(path = x,
                                records = ['ATOM'],
                                gz = False,
                                append_newline = True)

        if self.name == 'HETATM':
            with open(x, 'w') as newfile:
                self.carga.to_pdb(path = x,
                                records = ['HETATM'],
                                gz = False,
                                append_newline = True)

        if self.name == 'ANISOU':
            with open(x, 'w') as newfile:
                self.carga.to_pdb(path = x,
                                records = ['ANISOU'],
                                gz = False,
                                append_newline = True)

        if self.name == 'OTHERS':
            with open(x, 'w') as newfile:
                self.carga.to_pdb(path = x,
                                records = ['OTHERS'],
                                gz = False,
                                append_newline = True)

    # CARGAR ARCHIVO COMPLETO
    def cargar(self, x):
        self.carga = PandasPdb()
        self.carga.read_pdb(x)
        self.caracteres.set_text('%s\n...' % self.carga.pdb_text[:1000])
        self.entrega_inf()

    # DEFINIRPARA CADA PROTEINA INGRESADA 'FUNCIONES HACIA ABAJO'
    def inf_atom(self):
        self.ATOM = self.carga.df['ATOM']
        self.largo = len(self.ATOM.columns)
        self.modelo = Gtk.ListStore(*(self.largo*[str]))
        self.seleccion.set_model(self.modelo)
        cell = Gtk.CellRendererText()

        for i in range(self.largo):
            columns = Gtk.TreeViewColumn(self.ATOM.columns[i], cell,
                                         text = i)
            self.seleccion.append_column(columns)

        for value in self.ATOM.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_hetatm(self):
        self.HETATM = self.carga.df['HETATM']
        self.largo = len(self.HETATM.columns)
        self.modelo = Gtk.ListStore(*(self.largo*[str]))
        self.seleccion.set_model(self.modelo)
        cell = Gtk.CellRendererText()

        for i in range(self.largo):
            columns = Gtk.TreeViewColumn(self.HETATM.columns[i], cell,
                                         text = i)
            self.seleccion.append_column(columns)

        for value in self.HETATM.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_anisou(self):
        self.ANISOU = self.carga.df['ANISOU']
        self.largo = len(self.ANISOU.columns)
        self.modelo = Gtk.ListStore(*(self.largo*[str]))
        self.seleccion.set_model(self.modelo)
        cell = Gtk.CellRendererText()

        for i in range(self.largo):
            columns = Gtk.TreeViewColumn(self.ANISOU.columns[i], cell,
                                         text = i)
            self.seleccion.append_column(columns)

        for value in self.ANISOU.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    def inf_others(self):
        self.OTHERS = self.carga.df['OTHERS']
        self.largo = len(self.OTHERS.columns)
        self.modelo = Gtk.ListStore(*(self.largo*[str]))
        self.seleccion.set_model(self.modelo)
        cell = Gtk.CellRendererText()

        for i in range(self.largo):
            columns = Gtk.TreeViewColumn(self.OTHERS.columns[i], cell,
                                         text = i)
            self.seleccion.append_column(columns)

        for value in self.OTHERS.values:
            row = [str(x) for x in value]
            self.modelo.append(row)

    # ENTREGAR DETALLES OTHER
    def entrega_inf(self):
        self.detalles = self.carga.df['OTHERS'].copy()
        self.a = self.detalles[self.detalles['record_name'] == 'AUTHOR']
        self.entregas = self.a.values
        autores = ''
        for autor in self.entregas:
            autores += autor[1] + '\n'

        self.b = self.detalles[self.detalles['record_name'] == 'TITLE']
        self.entregas2 = self.b.values
        Title = ''
        for titles in self.entregas2:
            Title += titles[1] + '\n'

        self.c = self.detalles[self.detalles['record_name'] == 'SOURCE']
        self.d = self.c.values
        self.entregas3 = self.d[:5]
        source = ''
        for sources in self.entregas3:
            source += sources[1] + '\n'

        self.autor.set_text(f'Detalles:\n\nAutor(es): \n{autores}\nTitulo:'
                            f'\n{Title}\n'
                            f'Fuente(Primeros 5 SOURCE): \n{source}  ...')

    # DEFINIR EL COMBOBOX DEL TEXTO:
    def combobox_text_cambio(self, btn = None, opt = None):
        self.name = self.comboboxtext.get_active_text()

        if self.name == 'ATOM':
            self.inf_atom()

        if self.name == 'HETATM':
            self.inf_hetatm()

        if self.name == 'ANISOU':
            self.inf_anisou()

        if self.name == 'OTHERS':
            self.inf_others()

    def imagen_proteina(self):
        name = self.nombre
        filename = self.new
        aux = name.split('.')
        new_name = aux[0]
        imagen_g = "".join([filename])
        new_r = filename
        pymol.cmd.load(new_r, new_name)
        pymol.cmd.disable('all')
        pymol.cmd.enable(new_name)
        pymol.cmd.hide('all')
        pymol.cmd.show('cartoon')
        pymol.cmd.set('ray_opaque_background', 0)
        pymol.cmd.pretty(new_name)
        pymol.cmd.png(imagen_g)
        pymol.cmd.ray()
        self.imagen.set_from_file(f'{imagen_g}.png')


if __name__ == "__main__":
    MainWindow()
    Gtk.main()

