import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MainChooser():

    def __init__(self, option = None):
        builder = Gtk.Builder()
        builder.add_from_file('./ui/MainWindow.ui')
        self.dialogo = builder.get_object('FileChooser')

        if option == 'open':
            self.dialogo.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN,
                                     Gtk.ResponseType.OK)

        if option == 'save':
            self.dialogo.set_action(Gtk.FileChooserAction.SAVE)
            self.dialogo.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     "Guardar",
                                     Gtk.ResponseType.ACCEPT)

        self.dialogo.show_all()
