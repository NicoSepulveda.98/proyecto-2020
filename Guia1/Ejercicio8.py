#variable que ingresa el valor de las personas que contrata la empresa
personas_c = int(input("Ingrese cuantas personas contratadas son: "))

año_persona= 0

#un for para ir de persona en persona y pregunta el año que nacio y guardarlo
for i in range(1, personas_c + 1):
    edad_persona= int(input("En qué año nacio la persona {}: ". format(i)))
    resta_edad = 2020 - edad_persona                  #se establecio fecha 2020 (actual) para establecer su edad
    if (edad_persona  >= 2003):
        print("Fecha de año invalida. ¡Ingresar fecha de un mayor de edad!")
        print("El programa sera cerrado. Intentelo de nuevo")
        break

    else:
        resta_edad = 2020 - edad_persona                  #se establecio fecha 2020 (actual) para establecer su edad
        print("La edad de la persona n° {} es: ". format(i), resta_edad)
        año_persona = año_persona + resta_edad

#ecuación para sacar el promedio de edad
promedio_edad= año_persona / personas_c
print("El promedio de edad es: ", promedio_edad)

