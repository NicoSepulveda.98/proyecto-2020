#lista que guarde las horas diarias y ganancias que trabaje durante la semana
hora_diaria= []
ganancia= []
semana= 6
#variables para determinar los numeros mayores de la lista
dia_mayor = 0
dia_menor = 0
#variable para valor total
suma_diaria= 0

print("Le recordamos que la paga por hora en la empresa son de 1000.")
for i in range(1, semana + 1 ):
    horas= int(input("Por favor ingrese las horas trabajadas el dia {}: ". format(i)))
    hora_diaria.append(horas) #guardar en lista
    ganado_horas= horas * 1000
    print("Usted a ganado este día por horas trabajadas un valor de: ", ganado_horas)
    suma_diaria= suma_diaria + ganado_horas
    ganancia.append(ganado_horas) #guardar en lista

print ("La ganancia que obtuvo en la semana fue de: ", suma_diaria)

#pasamos la variables a un igual con lista y su max y min
dia_mayor = max(ganancia)
dia_menor = min(ganancia)
#imprimir cada variable
print ("El día que obtuvo ganancia mayor fue: ", dia_mayor)
print ("El día que obtuvo ganancia menor fue: ", dia_menor)
    
