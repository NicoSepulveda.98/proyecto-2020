#Proceso
#Analizar los numeros pares
#Determinar un recorrido o rango, es decir, mediante un for
#Determinar que rango tendra el analisis, tanto de partida como de final
#Al ser numeros pares, en el recorrido buscar una condicion
#Expresar la condición mediante un if determinando condicionar y "almacenar" los numeros pares
#Imprimir los numeros pares establecidos en el recorrido del for y condicionados en el if

for i in range(0, 100 +1):
    if (i % 2 == 0):
        print(i)
