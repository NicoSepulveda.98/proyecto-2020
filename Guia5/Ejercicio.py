texto = "co2_emission.csv"
archivo = open(texto, "r")
linea = archivo.readlines()
linea = linea[1:]


def abrir (linea):

    lista = []

    for i in linea:
        vacio = ""
        for j in i:
            vacio += j
        lista.append(vacio[:-2])

    linea = []

    for i in lista:
        arch = i.split(",")
        linea.append(arch)

    return linea


def maxco2 (var):

    acumular_valor = []

    for i in var:
        valor = i[3] #valor tonelada
        if valor != "": #vacio o tiene valor
            acumular_valor.append(float(valor)) #añade el valor a la lista como float

    valor_max = max(acumular_valor) #calcula valor maximo
    posicion = acumular_valor.index(valor_max) #determinar posicion del valor maximo para el pais
    out = var[posicion][0] #mostrar pais
    print("Valor maximo de toneladas de CO2: ", var[posicion])

    return var[posicion]


def minco2(var):

    acumular_valor = []

    for i in var:
        valor = i[3]
        if valor != "":
            acumular_valor.append(float(valor))

    valor_min = min(acumular_valor)
    posicion = acumular_valor.index(valor_min)
    out = var[posicion][0]
    print ("Valor minimo de toneladas de CO2: ", var[posicion])

    return var[posicion]

def promedio (var):


    acumular_valor = []

    for i in var:
        valor = i[3]
        if valor != "":
            acumular_valor.append(float(valor))

    division = len(acumular_valor)
    suma = sum(acumular_valor)

    if division != 0:
        media = suma /division

    else:
        media = 0



    return media

def sumaco2 (var):
    acumular_valor = []

    for i in var:
        valor = i[3]
        if valor != "":
            acumular_valor.append(float(valor))

    return acumular_valor


def promedio_pais(var):

    pais = []

    for i in var:
        pais.append(i[0])

    acumular_valor = [] #nombres
    acumular_valor2 = []
    count = [] #muestras por pais

    while len(pais) != 0:
        a = pais[0]
        llenar = pais.count(a)
        count.append(llenar)
        acumular_valor.append(a)
        pais = pais[llenar:]


    for i in count:
        llenar2 = promedio(var[:i-1])
        var = var[i:]
        acumular_valor2.append(llenar2)

    maxv = max(acumular_valor2)
    posicion = acumular_valor2.index(maxv)

    print ("El pais con mas promedio de toneladas es : " + acumular_valor[posicion])
    return acumular_valor[posicion]



def min_pais (var):

    pais = []

    for i in var:
        pais.append(i[0])

    acumular_valor = [] #nombres
    acumular_valor2 = []
    count = [] #muestras por pais

    while len(pais) != 0:
        a = pais[0]
        llenar = pais.count(a)
        count.append(llenar)
        acumular_valor.append(a)
        pais = pais[llenar:]


    for i in count:
        llenar2 = promedio(var[:i-1])
        var = var[i:]
        acumular_valor2.append(llenar2)

    minv = min(acumular_valor2)
    posicion = acumular_valor2.index(minv)

    print ("El pais con menos promedio de toneladas es : " + acumular_valor[posicion])
    return acumular_valor[posicion]


def margen (var):
    prom = promedio (var)
    inf = prom - prom * 0.1
    sup = prom + prom * 0.1

    pais = []

    for i in var:
        pais.append(i[0])

    acumular_valor = []
    acumular_valor2 = []
    count = []

    while len(pais) != 0:
        a = pais[0]
        llenar = pais.count(a)
        count.append(llenar)
        acumular_valor.append(a)
        pais = pais[llenar:]

    out = []
    for i in count:
        llenar2 = promedio (var[:i-1])
        var = var[i:]
        acumular_valor2.append(llenar2)

    for i in acumular_valor2:
        if i > inf and i < sup:
            posicion = acumular_valor2.index(i)
            out.append([acumular_valor[posicion], acumular_valor2[posicion]])
    print (out)

    return out

def main ():
    var = abrir(linea)
    maxco2(var)
    minco2(var)
    print ("Promedio de toneladas emitidas de CO2: " + str(promedio(var)))
    promedio_pais(var)
    min_pais(var)
    margen(var)



main()
