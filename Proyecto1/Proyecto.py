from os import system
from random import randint

def definir():
    imprimir = ["|"]
    for i in range(20):
        imprimir += (" ")
    imprimir.append("|")

    tab = []
    while len(tab) != 20:
        tablero = []
        for j in imprimir:
            tablero.append(j)
        tab.append(tablero)
    return tab


def mostrar_mapa(tab):
    for i in tab:
        z = ""
        for j in i:
            z += j
        print(z)


def camino_aleatorio(tab):
    a = 0
    b = 1
    xy = [a, b]
    while xy != [19, 20]:
        mov = randint(0, 3) # genera numero aleatorio del 0 al 3
        if mov == 0 or mov == 2:
            count = 0
            while count != 1:
                if mov == 0: 
                    a -= 1
                    if a == -1: # no salir de bordes
                        a = 0

                elif mov == 2:
                    b -= 1
                    if b == 0 or b == -1:
                        b = 1
                count += 1
                xy = [a,b]
                tab[a][b] = "*"

        else:
            count = 0
            while count != 5: # Se condiciona para que el camino sea mas recto
                if mov == 1:
                    a += 1 # camino hacia abajo
                    if a == 21 or a == 20:
                        a = 19
                else:
                    b += 1 # camino hacia la derecha
                    if b == 22 or b == 21:
                        b = 20
                count += 1
                tab[a][b] = "*" # posiciones y forma que tomara el camino
                xy = [a,b]
    return tab


# Movimiento de X segun lo ingresado por el ususario
def entrada(tab):
    a = 0
    b = 1
    xy = [a, b]
    cont = 0
    while xy != [19, 20]:
        ent = input("Para avanzar presione:\nA: Izquierda\n"
                    "D: Derecha\nS: Abajo\nW: Arriba\n")
        ref = []
        for i in xy:
            ref.append(i) # Guarda la posicion de xy antes del cambio
        if ent.upper() == "A":
            b -= 1
            cont += 1
        elif ent.upper() == "D":
            b += 1
            cont += 1
        elif ent.upper() == "W":
            a -= 1
            cont += 1
        elif ent.upper() == "S":
            a += 1
            cont += 1

        system("clear")
        aux = movimiento([a, b], tab, ent, ref)
        tab = aux[0]
        a = aux[1][0]
        b = aux[1][1]
        xy = [a, b]
        print(xy)
        mostrar_mapa(tab)
    print(f"Usted realizo {cont} movimientos")
    reiniciar()
    return ent


#Reemplaza x por * y viceversa
def movimiento(xy, tab, ent, ref):
    a = xy[0]
    b = xy[1]
    if a == len(tab) or a == -1:
        return[tab, ref]
    elif tab[a][b] == "*":
        tab[a][b] = "X"
        if ent.upper() == "S":
            tab[a-1][b] = "*"
        elif ent.upper() == "W":
            tab[a+1][b] = "*"
        elif ent.upper() == "A":
            tab[a][b+1] = "*"
        elif ent.upper() == "D":
            tab[a][b-1] = "*"
        return [tab, xy]
    else:
        print("No existe camino por ese lugar")
        return [tab, ref]


# Menu con opciones luego de llegar a la ultima coordenada
def reiniciar():
    tecla = input("Presione R para volver a jugar.\n"
                    "Presione F para finalizar.\n")
    if tecla.upper() == "R":
        tab = definir()
        tab = camino_aleatorio(tab)
        tab[0][1] = "X"
        mostrar_mapa(tab)
        entrada(tab)
    elif tecla.upper() == "F":
        print("¡Programa finalizado!")

# Main
a = 0
b = 1
xy = [a, b]
mov = randint(0, 3)
tab = definir()
tab = camino_aleatorio(tab)
tab[0][1] = "X"
mostrar_mapa(tab)
entrada(tab)
