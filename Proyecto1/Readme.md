**Guiate por el camino**

Este proyecto consiste en elaborar caminos aleatorios a partir de listas vacías, donde el jugador o el usuario, desde un punto inicial hasta un punto final, deberá 
recorrer uno de los caminos creados aleatoriamente. 
Una vez que el jugador o usuario llegue al punto final, se desplegará un mensaje que informará el número de movimientos que realizó hasta llegar a su destino, 
recorriendo el camino, el cual se ilustra con asteriscos. Pero, no solo eso, sino que además se mostrará un menú donde le darán a elegir dos opciones, las cuales son:
reiniciar, la que consiste en empezar de cero en el punto inicial con un camino aleatorio nuevo; finalizar, presionar f, que indica que el programa finalizó.

**Según la guía de estilo para códigos Python (PEP8)**

- Para diseño del código se debe usar 4 espacios por identificación y no mezclar tabulaciones con espacios.
- Cada línea puede extenderse hasta un máximo de 79 caracteres.
- Separar funciones y definiciones por dos líneas en blanco.
- Las importaciones deben estar en líneas separadas y agrupadas en orden: importaciones de librería estándar, importaciones terceras relacionadas,importaciones locales de la aplicación/librería.
- Evita usar espacios en blanco inmediatamente dentro de un paréntesis, corchetes, llaves, antes de una coma, punto y coma, dos puntos, antes del paréntesis que comienza la lista de argumentos, más de un espacio alrededor de un operador de asignación.
- Siempre rodear con espacios a cada lado con operadores binarios.
- Evitar nombrar variables con los caracteres O (o mayúscula), letra l (L minúscula) e I (i mayúscula).
- Utilizar guión bajo o doble guión bajo como prefijo y sufijo como estilo de nombramiento ya sea para variables o definición de funciones o procedimientos.


**PEP8 en nuestro código**

- No se extendió de los 79 caracteres por línea.
- Separación de funciones y definiciones por dos líneas en blanco.
- Separación por líneas distintas de las importaciones utilizadas.
- Se evitó la utilización de los espacios en blanco (paréntesis, corchetes, llaves, entre otros)
- Se ocupó los espacios a cada lado en las operaciones binarias.
- No se utilizó los nombres de variables mencionados (O,l,i)


**Construido con**

Este proyecto de programación fue construido bajo el lenguaje de programación multiparadigma Python.


**Pre-requisitos**

Para ejecutar el código debe tener instalada la versión python 3. Si no lo tiene instalado a continuación le dejamos los pasos a seguir para instalarlo.

Si estás usando Ubuntu, puedes instalar python 3 con los siguientes comandos:


```
sudo apt-get update
sudo apt-get install python3
```



Si estás usando debian 8 o anteriores, puedes instalar python con el siguiente comando:
`apt-get install python3`

Si estás usando debian 9, puedes instalar python con el siguiente comando:
`apt install python3`


Si ocupa otro sistema operativo como Windows, puede optar por instalar python idle versión 3.8 u otro entorno de desarrollo integrado para Python.

**Instalación**

Debe descargar el código el cual se encuentra en alojo de proyectos con utilización de sistema de control de versiones Git (GitLab)
Una vez hecho esto debe abrir una terminal donde se guardó el archivo y escribir el siguiente comando:

`python3 Proyecto.py`

**Autores**

Nicolás Sepúlveda - Estudiante de segundo año de Ingeniería Civil en Bioinformática.

Simone Urrutia - Estudiante de segundo año de Ingeniería Civil en Bioinformática.

 	


	  
