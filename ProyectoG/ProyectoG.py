import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from archivo_completo import MainData
from Hipotesis_A import MainHipotesis_A
from Hipotesis_B import MainHipotesis_B
from Hipotesis_C import MainHipotesis_C
from Hipotesis_D import MainHipotesis_D
from Hipotesis_E import MainHipotesis_E

class MainWindow():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/Window.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Analisis de Pandemia")
        self.window.set_default_size(800, 600)

        #COMBOBOX DE PANTALLA INICIAL
        self.combobox = self.builder.get_object("combobox")
        self.lista = Gtk.ListStore(str)
        self.hipotesis_texto = self.builder.get_object("hipotesis")
        self.entrega_respuesta_texto = self.builder.get_object("respuesta_hip")

        self.hipotesis = {"Hipotesis A": "El Top 5 de los paises con mas porcentaje de"
                          " contagio por COVID son, por consecuencia, los paises"
                          " con mayor cantidad de contagio en sus habitantes."
                          "\nIncluyendo los recuperados, muertos y activos.",
                          "Hipotesis B": "¿Chile posee la dieta entregada por USDA?."
                          "\n¿Permite que obtenga resultados positivos en relacion a"
                          " los recuperados por COVID, estando arriba del promedio"
                          " general de recuperados? ¿Qué tipo de dieta sigue Chile?",
                          "Hipotesis C": "¿Cuáles son los 5 paı́ses que tienen más"
                          " habitantes con obesidad?",
                          "Hipotesis D": "¿Cuáles son los 5 paı́ses que tienen más"
                          " habitantes con obesidad y que además lo que más"
                          " muertes tienen?",
                          "Hipotesis E": "¿Los paı́ses que tienen mayor consumo de"
                          " alcohol son los paı́ses que más contagios"
                          " confirmados tienen?"}


        self.entrega_respuesta = {"Hipotesis A": "Se concluye que el Top 5"
                                  " de porcentaje es diferente"
                                  " \nal de mayor cantidad de habitantes de contagio por COVID."
                                  " \nEl que mejor representa la tabla de Contagio es"
                                  " de Habitantes C., ya que cada pais posee diferente"
                                  " \ncantidad de poblacion.\n",
                                  "Hipotesis B": "Chile no sigue la dieta entregada por USDA."
                                  " Sin embargo, su calificación lo hace estar entre\n"
                                  " el Top 5 de los paises con mayor porcentaje de"
                                  " recuperados por COVID, \nademás de estar por"
                                  " arriba del promedio"
                                  " general de los paises. \nPor lo tanto,"
                                  " la dieta no es un factor"
                                  " influyente a la recuperacion.\nAl"
                                  " examinar su dieta, refleja un alto"
                                  " consumo de Verduras y"
                                  " Proteinas, \na su vez un bajo"
                                  " nivel de consumo de frutas,"
                                  " con un porcentaje moderado"
                                  " de ingesta de granos.",
                                  "Hipotesis C": "Dando como visto cuales"
                                  " son los 5 paises"
                                  " con mayor habitantes con rasgos de Obesidad",
                                  "Hipotesis D": "Se refleja en cada tabla los paises"
                                  " distintos entre cada resultado."
                                  " Sin embargo entrega 3 paises iguales.",
                                  "Hipotesis E": "Se concluye que el Top 10"
                                  " de los mayores confirmados"
                                  " y los Top 10 que mas beben alcohol, no son"
                                  " en semejanza de paises."}

        #CARGAR LOS CAMBIOS DE LAS HIPOTESIS
        self.combobox.connect("changed", self.cargar_hipotesis)

        cell = Gtk.CellRendererText()
        nombres = ["Hipotesis A", "Hipotesis B", "Hipotesis C", "Hipotesis D", "Hipotesis E"]

        self.combobox.pack_start(cell, True)
        self.combobox.add_attribute(cell, 'text', 0)

        for nombre in nombres:
            self.lista.append([nombre])

        self.combobox.set_model(self.lista)

        #BOTON PARA ENTRADA A LAS HIPOTESIS
        boton = self.builder.get_object("btn_entrar")
        boton.connect("clicked", self.on_btn)
        boton.set_label("Entrar")

        #BOTON PARA ENTRADA AL ARCHIVO COMPLETO
        boton2 = self.builder.get_object("btn_entrar_pandemia")
        boton2.connect("clicked", self.on_btn_pandemia)
        boton2.set_label("Entrar")

        self.window.show_all()

    #BOTON ENTRADA AL ARCHIVO COMPLETO
    def on_btn_pandemia (self, btn = None, opt = None):
        self.nueva_ventana = MainData()

    #CARGA DE LOS NOMBRES DE LA HIPOTESIS EN COMBOBOX
    def cargar_hipotesis(self, btn = None):

        iterador = self.combobox.get_active_iter()
        modelo = self.combobox.get_model()
        # De lo anterior se obtiene nombre
        self.nombre = modelo[iterador][0]
        # Se presenta nombre en el label creado
        self.hipotesis_texto.set_text("La hipotesis seleccionada dice: "
                                     f"{self.hipotesis[self.nombre]}")

        self.entrega_respuesta_texto.set_text("La respuesta de Hipotesis dice: "
                                             f"{self.entrega_respuesta[self.nombre]}")


    #COMPARACION DE LAS HIPOTESIS
    def on_btn (self, btn = None):

        if self.nombre == 'Hipotesis A':
            self.nueva_HipotesisA = MainHipotesis_A()

        if self.nombre == 'Hipotesis B':
            self.nueva_HipotesisB = MainHipotesis_B()

        if self.nombre == 'Hipotesis C':
            self.nueva_HipotesisC = MainHipotesis_C()

        if self.nombre == 'Hipotesis D':
            self.nueva_HipotesisD = MainHipotesis_D()

        if self.nombre == 'Hipotesis E':
            self.nueva_HipotesisD = MainHipotesis_E()

if __name__ == "__main__":
    MainWindow()
    Gtk.main()
