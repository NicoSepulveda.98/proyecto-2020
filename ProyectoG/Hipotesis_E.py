import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainHipotesis_E():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisE.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Muestra del resultado Hipotesis E")

        self.btn_return = self.builder.get_object("btn_volver")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")


        self.pandastree = self.builder.get_object("pandas_tree")
        self.pandastree2 = self.builder.get_object("pandas_tree2")


        self.entrega_analisis = self.builder.get_object("entrega_hip")

        self.archivo = get_dataframe()

        self.window.show_all()

        a = self.archivo[['Country', 'Alcoholic Beverages', 'Unit (all except Population)']]
        b = a.sort_values(by = 'Alcoholic Beverages', ascending = False)
        self.confirmado = b.head(10)

        #MOSTRAR LAS COMUNAS DEL DATA
        len_columns = len(self.confirmado.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)

        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.confirmado.columns[item], cell, text = item)
            self.pandastree.append_column(column)


        for value in self.confirmado.values:
            row = [str(x) for x in value]
            modelo.append(row)

        xa = self.archivo[['Country', 'Confirmed', 'Unit (all except Population)']]
        xb = xa.sort_values(by = 'Confirmed', ascending = False)
        self.confirmado2 = xb.head(10)

        #MOSTRAR LAS COMUNAS DEL DATA
        len_columns2 = len(self.confirmado2.columns)
        modelo2 = Gtk.ListStore(*(len_columns2 * [str]))
        self.pandastree2.set_model(model = modelo2)
        cell2 = Gtk.CellRendererText()

        for item in range(len_columns2):
            column2 = Gtk.TreeViewColumn(self.confirmado2.columns[item], cell2, text = item)
            self.pandastree2.append_column(column2)

        for value in self.confirmado2.values:
            row2 = [str(x2) for x2 in value]
            modelo2.append(row2)

        self.entrega_analisis.set_text("Se concluye que el Top 10 de los mayores confirmados"
                                       " y los Top 10 que mas beben alcohol, no son"
                                       " en semejanza de paises.")

    def volver(self, btn = None):
        self.window.destroy()

if __name__ == "__main__":

    MainHipotesis_E()
    Gtk.main()
