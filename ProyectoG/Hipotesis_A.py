import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainHipotesis_A():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisA.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        #self.window.maximize()
        self.window.set_title("Muestra del resultado Hipotesis A")

        self.btn_return = self.builder.get_object("btn_volver")
        self.pandastree = self.builder.get_object("pandas_tree") #TABLA 1

        #self.grafica = self.builder.get_object("grafico_confirmados") #GRAFICA 1
        #self.grafica.set_from_file("grafico1.png")

        self.pandastree2 = self.builder.get_object("pandas_tree2")  #TABLA 2


        self.entrega_analisis = self.builder.get_object("respuesta_hip")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")

        self.dataframe = get_dataframe()


        self.window.show_all()

  #  def muestra_hipotesis_confirmados(self, btn = None):
        a = self.dataframe[['Country', 'Confirmed', 'Unit (all except Population)']]
        b = a.sort_values(by = 'Confirmed', ascending = False)
        self.confirmado = b.head()

        #MOSTRAR LAS COMUNAS DEL DATA
        len_columns = len(self.confirmado.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)

        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.confirmado.columns[item], cell, text = item)
            self.pandastree.append_column(column)


        for value in self.confirmado.values:
            row = [str(x) for x in value]
            modelo.append(row)

        #self.grafica = self.confirmado.plot(kind = 'bar', stacked = True, x = 'Country')
        #plt.ylabel('(%) Confirmados')
        #plt.title('Top 5 Paises Confirmados')
        #plt.savefig("grafico1.png")






        #def muestra_hipotesis_habitantes(self, btn = None):
        x = self.dataframe['Confirmed']
        y = self.dataframe['Population']
        z = pd.DataFrame(x * y // 100, columns = ['Contagios Habitantes'])
        self.dataframe = pd.concat([self.dataframe, z], axis = 1)
        c = self.dataframe
        xa = c[['Country', 'Contagios Habitantes']]
        xb = xa.sort_values(by = 'Contagios Habitantes', ascending = False)
        self.confirmado2 = xb.head()

        len_columns2 = len(self.confirmado2.columns)
        modelo2 = Gtk.ListStore(*(len_columns2 * [str]))
        self.pandastree2.set_model(model = modelo2)

        cell2 = Gtk.CellRendererText()

        for item in range(len_columns2):
            column2 = Gtk.TreeViewColumn(self.confirmado2.columns[item], cell2, text = item)
            self.pandastree2.append_column(column2)


        for value in self.confirmado2.values:
            row2 = [str(x2) for x2 in value]
            modelo2.append(row2)

        self.entrega_analisis.set_text("Se concluye que el Top 5 de porcentaje es diferente"
                                       " \nal de mayor cantidad de habitantes de contagio por COVID."
                                       " \nEl que mejor representa la tabla de Contagio es"
                                       " de Habitantes C., ya que cada pais posee diferente"
                                       " \ncantidad de poblacion.\n")

    def volver(self, btn = None):
        self.window.destroy()

if __name__ == "__main__":

    MainHipotesis_A()
    Gtk.main()
