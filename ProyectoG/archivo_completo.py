import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainData():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/arch_completo.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Archivo Completo mundia de Pandemia")

        self.btn_return = self.builder.get_object("btn_volver")
        self.pandastree = self.builder.get_object("pandas_tree")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")

        self.dataframe = get_dataframe()

        #MOSTRAR LAS COMUNAS DEL DATA
        len_columns = len(self.dataframe.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)
        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.dataframe.columns[item], cell, text = item)
            self.pandastree.append_column(column)

        for value in self.dataframe.values:
            row = [str(x) for x in value]
            modelo.append(row)

        self.window.show_all()

    def volver(self, btn = None):
        self.window.destroy()

if __name__ == "__main__":

    MainData()
    Gtk.main()
