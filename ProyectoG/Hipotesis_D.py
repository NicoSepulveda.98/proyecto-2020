import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainHipotesis_D():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisD.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Muestra del resultado Hipotesis D")

        self.btn_return = self.builder.get_object("btn_volver")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")
        #GRAFICO 1
        #self.btn_grafic = self.builder.get_object("btn_grafic")
        #self.btn_grafic.connect ("clicked")
        #self.btn_grafic.set_label("Ir Grafica")
        #GRAFICO 2
        #self.btn_grafic2 = self.builder.get_object("btn_grafic2")
        #self.btn_grafic2.connect("clicked")
        #self.btn_grafic2.set_label("Ir Grafica")

        self.pandastree = self.builder.get_object("pandas_tree") #TABLA 1
        self.pandastree2 = self.builder.get_object("pandas_tree2")

        self.entrega_hipotesis = self.builder.get_object("entrega_hip")

        self.archivo = get_dataframe()

        self.window.show_all()

        #TABLA DE LA OBESIDAD EN HABITANTES POR PAIS
        x = self.archivo['Obesity']
        y = self.archivo['Population']
        #ECUACION PARA ENCONTRAR LA CANTIDAD EXACTA DE HABITANTES
        z = pd.DataFrame(x * y / 100, columns = ['Obesidad Habitantes'])
        self.archivo = pd.concat([self.archivo, z], axis = 1)
        c = self.archivo
        a = c[['Country', 'Obesidad Habitantes']]
        b = a.sort_values(by = 'Obesidad Habitantes', ascending = False)
        self.confirmados = b.head()

        #self.grafica = self.confirmados.plot(kind = 'bar', stacked = True, x = 'Country')
        #plt.ylabel('Habitantes Obesos')
        #plt.title('Top 5 Paises Obesos (Habitantes)')

        len_columns = len(self.confirmados.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)
        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.confirmados.columns[item], cell, text = item)
            self.pandastree.append_column(column)

        for value in self.confirmados.values:
            row = [str(x) for x in value]
            modelo.append(row)

        #ENTREGA DE LA TABLA DE MUERTES HABITANTES POR COVID
        xx = self.archivo['Deaths']
        yy = self.archivo['Population']
        #ECUACION PARA ENCONTRAR LA CANTIDAD EXACTA DE HABITANTES
        zz = pd.DataFrame(xx * yy // 100, columns = ['Muertes Habitantes'])
        self.archivo = pd.concat([self.archivo, zz], axis = 1)
        xc = self.archivo
        xa = xc[['Country', 'Muertes Habitantes']]
        xb = xa.sort_values(by = 'Muertes Habitantes', ascending = False)
        self.confirmados2 = xb.head()

        len_columns2 = len(self.confirmados2.columns)
        modelo2 = Gtk.ListStore(*(len_columns2 * [str]))
        self.pandastree2.set_model(model = modelo2)
        cell2 = Gtk.CellRendererText()

        for item in range(len_columns2):
            column2 = Gtk.TreeViewColumn(self.confirmados2.columns[item], cell2, text = item)
            self.pandastree2.append_column(column2)

        for value in self.confirmados2.values:
            row2 = [str(x2) for x2 in value]
            modelo2.append(row2)

        self.entrega_hipotesis.set_text("Se refleja en cada tabla los paises"
                                        " distintos entre cada resultado."
                                        " Sin embargo entrega 3 paises iguales.")
    def volver(self, btn = None):
        self.window.destroy()

if __name__ == "__main__":

    MainHipotesis_D()
    Gtk.main()
