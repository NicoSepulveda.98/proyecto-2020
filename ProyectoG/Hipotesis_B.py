import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainHipotesis_B():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisB.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Muestra del resultado Hipotesis B")

        #LLAMAR A LA FUNCION DEL BOTON VOLVER
        self.btn_return = self.builder.get_object("btn_volver")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")

        self.pandastree = self.builder.get_object("pandas_tree")
        self.pandastree2 = self.builder.get_object("pandas_tree2")
        self.pandastree3 = self.builder.get_object("pandas_tree3")

        self.comparacion_texto = self.builder.get_object("comparacion_realizada")
        self.entrega_hipotesis = self.builder.get_object("entrega_hip")

        self.archivo = get_dataframe()

        self.window.show_all()

        lista = []
        grano = self.archivo.iloc[29, 1] + self.archivo.iloc[29, 5] + self.archivo.iloc[29, 13]\
        + self.archivo.iloc[29, 14] + self.archivo.iloc[29, 15]
        verd = self.archivo.iloc[29, 16] + self.archivo.iloc[29, 18] + self.archivo.iloc[29, 19]\
        + self.archivo.iloc[29, 21] + self.archivo.iloc[29, 22] + self.archivo.iloc[29, 23]
        frut = self.archivo.iloc[29, 8] + self.archivo.iloc[29, 17] + self.archivo.iloc[29, 20]
        prot = self.archivo.iloc[29, 2] + self.archivo.iloc[29,3] + self.archivo.iloc[29, 4]\
        + self.archivo.iloc[29, 6] + self.archivo.iloc[29, 7] + self.archivo.iloc[29, 9]\
        + self.archivo.iloc[29, 10] + self.archivo.iloc[29, 11] + self.archivo.iloc[29, 12]
        aux = [grano, verd, frut, prot]
        lista.append(aux)
        lista2 = []
        for i in lista:
            cond1 = bool()
            cond2 = bool()
            cond3 = bool()
            cond4 = bool()
            if i[0] > 25 and i[0] < 35:
                cond1 = True
            if i[1] > 35 and i[1] < 45:
                cond2 = True
            if i[2] > 5 and i[2] < 15:
                cond3 = True
            if i[3] > 15 and i[3] < 25:
                cond4 = True
            if cond1 == True and cond2 == True and cond3 == True and cond4 == True:
                cumple = True
            else:
                cumple = False
            aux2 = [cond1, cond2, cond3, cond4, cumple]
            lista2.append(aux2)
        self.valores = pd.DataFrame(lista, columns = ['Grano','Verdura',
                                                 'Fruta','Proteina'])
        self.new = pd.DataFrame(lista2, columns = ['Grano', 'Verdura', 'Fruta',
                                              'Proteina', 'Cumple dieta'])


        len_columns = len(self.valores.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)

        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.valores.columns[item], cell, text = item)
            self.pandastree.append_column(column)


        for value in self.valores.values:
            row = [str(x) for x in value]
            modelo.append(row)

        #VALORES 2
        len_columns2 = len(self.new.columns)
        modelo2 = Gtk.ListStore(*(len_columns2 * [str]))
        self.pandastree2.set_model(model = modelo2)

        cell2 = Gtk.CellRendererText()

        for item in range(len_columns2):
            column2 = Gtk.TreeViewColumn(self.new.columns[item], cell2, text = item)
            self.pandastree2.append_column(column2)


        for value in self.new.values:
            row2 = [str(x2) for x2 in value]
            modelo2.append(row2)

        rec = self.archivo['Recovered']
        rec = pd.concat([self.archivo['Country'], rec], axis = 1)
        rec = rec.sort_values(by = 'Recovered', ascending = False)
        self.rec = rec.head()
        self.promch = rec.iloc[4,1]

        #VALORES 3
        len_columns3 = len(self.rec.columns)
        modelo3 = Gtk.ListStore(*(len_columns3 * [str]))
        self.pandastree3.set_model(model = modelo3)

        cell3 = Gtk.CellRendererText()

        for item in range(len_columns3):
            column3 = Gtk.TreeViewColumn(self.rec.columns[item], cell3, text = item)
            self.pandastree3.append_column(column3)

        for value in self.rec.values:
            row3 = [str(x3) for x3 in value]
            modelo3.append(row3)

        self.aprom = self.archivo['Recovered'].mean()

        self.comparacion_texto.set_text("El promedio de Recuperado entre todos los paises"
                                        f" es: {self.aprom}\nEl promedio de Chile es: {self.promch}")

        self.entrega_hipotesis.set_text("Chile no sigue la dieta entregada por USDA."
                                       " Sin embargo, su calificación lo hace estar entre\n"
                                       " el Top 5 de los paises con mayor porcentaje de"
                                       " recuperados por COVID, \nademás de estar por"
                                       " arriba del promedio"
                                       " general de los paises. \nPor lo tanto,"
                                       " la dieta no es un factor"
                                       " influyente a la recuperacion.\nAl"
                                       " examinar su dieta, refleja un alto"
                                       " consumo de Verduras y"
                                       " Proteinas, \na su vez un bajo"
                                       " nivel de consumo de frutas,"
                                       " con un porcentaje moderado"
                                       " de ingesta de granos.")


    def volver(self, btn = None):
        self.window.destroy()

if __name__ == "__main__":

    MainHipotesis_B()
    Gtk.main()
