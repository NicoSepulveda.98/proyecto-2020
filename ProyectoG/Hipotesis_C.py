import pandas as pd
from os import system
import matplotlib.pyplot as plt
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# Función externa para carga de dataframe
def get_dataframe():

    dataframe = "suministro_alimentos_kcal.csv"
    data = pd.read_csv(dataframe, sep = ",")
    data = data.fillna(0)
    return data

class MainHipotesis_C():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisC.ui")

        self.window = self.builder.get_object("main_window")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Muestra del resultado Hipotesis C")

        self.btn_return = self.builder.get_object("btn_volver")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")


        #self.btn_grafic = self.builder.get_object("btn_grafic")
        #self.btn_grafic.connect("clicked", self.entrega, "open")
        #self.btn_grafic.set_label("Entrar Grafico")

        self.pandastree = self.builder.get_object("pandas_tree") #TABLA 1


        self.entrega_hipotesis = self.builder.get_object("entrega_hip")

        self.archivo = get_dataframe()

        self.window.show_all()

        x = self.archivo['Obesity']
        y = self.archivo['Population']
        #ECUACION PARA ENCONTRAR LA CANTIDAD EXACTA DE HABITANTES
        z = pd.DataFrame(x * y / 100, columns = ['Obesidad Habitantes'])
        self.archivo = pd.concat([self.archivo, z], axis = 1)
        c = self.archivo
        a = c[['Country', 'Obesidad Habitantes']]
        b = a.sort_values(by = 'Obesidad Habitantes', ascending = False)
        self.confirmados = b.head()

        len_columns = len(self.confirmados.columns)
        modelo = Gtk.ListStore(*(len_columns * [str]))
        self.pandastree.set_model(model = modelo)

        cell = Gtk.CellRendererText()

        for item in range(len_columns):
            column = Gtk.TreeViewColumn(self.confirmados.columns[item], cell, text = item)
            self.pandastree.append_column(column)

        for value in self.confirmados.values:
            row = [str(x) for x in value]
            modelo.append(row)

        self.entrega_hipotesis.set_text("Dando como visto cuales son los 5 paises"
                                        " con mayor habitantes con rasgos de Obesidad")

    def volver(self, btn = None):
        self.window.destroy()

    def confirmados_f (self, btn = None):
        self.confirmadosf = self.confirmados
        return self.confirmadosf
'''
#GRAFICA NO FUNCIONAL
class MainGrafic():



    def __init__(self):


        self.builder = Gtk.Builder()
        self.builder.add_from_file ("./ui/HipotesisC.ui")

        self.window = self.builder.get_object("window_grafic")
        self.window.connect("destroy", self.volver)
        self.window.set_default_size(800, 600)
        self.window.set_title("Muestra del Grafico Hipotesis C")


        #self.window.show_all()

        self.btn_return = self.builder.get_object("btn_volver_grafic")
        self.btn_return.connect("clicked", self.volver)
        self.btn_return.set_label("Volver")

    def grafica_open(self, btn = None):

        new = MainHipotesis_C()
        self.entrega = new.confirmados_f()

        self.grafica = self.entrega.plot(kind = 'bar', stacked = True, x = 'Country')
        plt.ylabel('Habitantes Obesos')
        plt.title('Top 5 Paises Obesos (Habitantes)')
        plt.savefig("Grafica de Habitantes Obesos.png")

        self.grafica = self.builder.get_object("grafic_c")
        self.grafica.set_from_file("Grafica de Habitantes Obesos.png")


    def volver(self, btn = None):
        self.window.destroy()
'''
if __name__ == "__main__":

    MainHipotesis_C()
    #MainGrafic()
    Gtk.main()
