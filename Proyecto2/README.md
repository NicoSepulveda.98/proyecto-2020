**Análisis de Datos en una Pandemia.**

Este proyecto consiste en la manipulación de un documento de texto con datos en su interior (dataset) para trabajar en él y así poder obtener respuestas a las cuatro hipótesis que se crearon.
Cuando se ejecute el programa se mostrará un menú el cual contiene las cuatro hipótesis o preguntas, según la que seleccione se le enseñará los resultados obtenidos del análisis de datos del dataset mediante tablas, gráficos y así llegar a una conclusión para confirmar o refutar la hipótesis seleccionada.
Las hipótesis o preguntas están enfocadas a una dieta dada por USDA y sobre el efecto que produjo el COVID-19 en la población de los países (contagios, muertes, recuperados, activos).
La dieta USDA consiste en 4 grupos de alimentos: Granos, Verduras, Frutas, Proteínas. Para establecer el orden de ellos y dar respuesta a quienes respetan dicha dieta.

- Los granos están constituidos por los siguientes alimentos: _Alcoholic Beverage, Cereals - Excluding Beer, Oilcrops, Pulses, Spices._

- Las verduras: _Starchy Roots, Sugar Crops, Sugar Sweeteners, Vegetal Products, Vegetal Oils, Vegetables._
 
- Las frutas: _Fruits - Excluding Wine, Stimulants, Treenuts._
 
- Las proteínas: _Animal Products, Animals Fats, Aquatic Product, Eggs, (Fish, Seafood), Miscellaneous, Offals._
 



**Construido con**

Este proyecto de programación fue construido bajo el lenguaje de programación multiparadigma Python, las librerías pandas y matplotlib.

**Pre-requisitos**

Para ejecutar el código debe tener instalada la versión python 3. Si no lo tiene instalado a continuación le dejamos los pasos a seguir para instalarlo.

Si está usando Ubuntu, puedes instalar python 3 con los siguientes comandos:

`sudo apt-get install python3`

Si está usando debian 8 o anteriores, puedes instalar python con el siguiente comando (es necesario ingresar como superusuario ante de proceder a escribir el comando):

`apt-get install python3`

Si está usando debian 10, puedes instalar python con el siguiente comando:

`apt install python3`

Si ocupa otro sistema operativo como Windows, puede optar por instalar python idle versión 3.8 u otro entorno de desarrollo integrado para Python.

Además como para crear este proyecto se hizo uso de la librería pandas, la cual también debe ser instalada, a continuación te guiamos como hacerlo:

**Instalación de pandas.**




Para Ubuntu:

`sudo apt-get install python3-pandas`




Para debian:

Es necesario ingresar como superusuario antes de ejecutar el comando.

`apt install python3-pandas`

Para Windows:

`install pandas`


**Instalación**

Debe descargar el código el cual se encuentra alojado en GitLab.
Una vez hecho esto, donde se guardó el archivo, debe abrir una terminal y escribir en ella el siguiente comando:

`python3 Proyecto2.py`

**Autores**

**Nicolás Sepúlveda** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.

**Simone Urrutia** - Estudiante de segundo año de Ingeniería Civil en Bioinformática.
