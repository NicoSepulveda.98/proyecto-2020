#!/usr/bin/env python3
# -- coding: utf-8 --

import pandas as pd
from os import system
import matplotlib.pyplot as plt

#INGRESAR EL ARCHIVO DE TEXTO
datos = pd.read_csv('suministro_alimentos_kcal.csv', sep = ",")
archivo = datos.fillna(0)

#HIPOTESIS A
def confirmados_pais(archivo):
    a = archivo[['Country', 'Confirmed', 'Unit (all except Population)']]
    b = a.sort_values(by = 'Confirmed', ascending = False)
    #MOSTRAR LOS PRIMEROS 5 PAISES
    confirmados = b.head()
    print(f"El top 5 paises de confirmados por (%) COVID son:\n{confirmados}")
    grafica = confirmados.plot(kind = 'bar', stacked = True, x = 'Country')
    plt.ylabel('(%)Confirmado')
    plt.title('Top 5 Paises Confirmados (%)')
    plt.show()


def top5_confirmados(archivo):
    x = archivo['Confirmed']
    y = archivo['Population']
    #ECUACION PARA ENCONTRAR LA CANTIDAD EXACTA DE HABITANTES
    z = pd.DataFrame(x * y / 100, columns = ['Contagios Habitantes'])
    archivo = pd.concat([archivo, z], axis = 1)
    c = archivo
    a = c[['Country', 'Contagios Habitantes']]
    b = a.sort_values(by = 'Contagios Habitantes', ascending = False)
    confirmados = b.head()
    print(f"El Top 5 de habitantes confirmados con COVID son:\n{confirmados}")
    grafica = confirmados.plot(kind = 'bar', stacked = True, x = 'Country')
    plt.ylabel('Habitantes Contagiados')
    plt.title('Top 5 Paises confirmados (Habitantes)')


#HIPOTESIS B
def obesidad_desnutricion(archivo):
    a = archivo[['Country', 'Obesity','Deaths',
                 'Unit (all except Population)']]
    b = a.sort_values(by = 'Obesity', ascending = False)
    obesidad = b.head(10)
    print(f"Los paises con mayor (%) de obesidad son:\n{obesidad}")
    print("\n")
    c = a.sort_values(by = 'Deaths', ascending = False)
    muerte = c.head(10)
    print(f"Los paises con mayor (%) de muerte por COVID son:\n{muerte}")
    print("\nSe observa al comparar los datos de ambas tablas"
          " una diferencia de paises, demostrada por la diferencia"
          " de muertes (%) y porcentaje de obesidad.")
    grafica1 = obesidad.plot(kind = 'bar', stacked = True, x = 'Obesity')
    plt.ylabel('(%) Muerte')
    plt.title('Top 10 Paises con mayor Obesidad'
              '\n junto a Muertes')
    grafica2 = muerte.plot(kind = 'bar', stacked = True, x = 'Deaths')
    plt.ylabel('(%) Obesidad')
    plt.title('Top 10 Paises con mayor Muerte'
              '\n junto a Obesidad')
    plt.show()
    #ELIMINAR LOS SIGNOS '<' DE LA COLUMNA DE DESNUTRICION
    datos_desnutricion = archivo['Undernourished']
    lista = []
    for i in datos_desnutricion:
        if type(i) == int:
            i = '0'
        if i[0] == '<':
            i = i[1:]
        i = float(i)
        lista.append(i)
    new = pd.DataFrame(lista, columns = ['Undernourished New'])
    archivo = pd.concat([archivo, new], axis = 1)
    xa = archivo[['Country', 'Undernourished New',
                  'Deaths', 'Unit (all except Population)']]
    xb = xa.sort_values(by = 'Undernourished New', ascending = False)
    desnutricion = xb.head(10)
    print("\n")
    print(f"Los paises con mayor (%) de desnutrición son:\n{desnutricion}")
    xc = xa.sort_values(by = 'Deaths', ascending = False)
    xmuerte = xc.head(10)
    print(f"Los paises con mas (%) de muerte son:\n{xmuerte} ")
    print("\nAl igual como sucedio en la comparacion de"
          " Obesidad y porcentaje de Muerte por COVID,"
          " en la desnutrición ocurre el mismo resultado."
          " No se refleja una proporcionalidad directa de"
          " desnutricion (%) y muerte (%). Reflejandose"
          " en la diferencia de los paises y datos.")
    grafica3 = desnutricion.plot(kind = 'bar', stacked = True,
                                 x = 'Undernourished New')
    plt.ylabel('(%) Muerte')
    plt.title('Top 10 Paises Mayor Desnutricion'
              '\n junto a Muertes')
    grafica4 = xmuerte.plot(kind = 'bar', stacked = True, x = 'Deaths')
    plt.ylabel('(%) Desnutricion')
    plt.title('Top 10 Paises Mayor Muerte'
              '\n junto a Desnutricion')
    plt.show()

#HIPOTESIS C
def porcentaje_dieta(archivo):
    lista = []
    #ENCONTRAR Y SEPARAR CADA COMIDA EN SU RESPECTIVO ESTADO(GRANO,VERDURA,ETC)
    for i in range(0, 170):
        grano = archivo.iloc[i, 1] + archivo.iloc[i, 5] + archivo.iloc[i, 13]\
        + archivo.iloc[i, 14] + archivo.iloc[i, 15]
        verd = archivo.iloc[i, 16] + archivo.iloc[i, 18] + archivo.iloc[i, 19]\
        + archivo.iloc[i, 21] + archivo.iloc[i, 22] + archivo.iloc[i, 23]
        frut = archivo.iloc[i, 8] + archivo.iloc[i, 17] + archivo.iloc[i, 20]
        prot = archivo.iloc[i, 2] + archivo.iloc[i,3] + archivo.iloc[i, 4]\
        + archivo.iloc[i, 6] + archivo.iloc[i, 7] + archivo.iloc[i, 9]\
        + archivo.iloc[i, 10] + archivo.iloc[i, 11] + archivo.iloc[i, 12]
        aux = [grano, verd, frut, prot]
        lista.append(aux)
    lista2 = []
    #CREAR BOOLS PARA ESTABLECER TRUE O FALSE
    for i in lista:
        cond1 = bool()
        cond2 = bool()
        cond3 = bool()
        cond4 = bool()
        if i[0] > 25 and i[0] < 35:
            cond1 = True
        if i[1] > 35 and i[1] < 45:
            cond2 = True
        if i[2] > 5 and i[2] < 15:
            cond3 = True
        if i[3] > 15 and i[3] < 25:
            cond4 = True
        if cond1 == True and cond2 == True and cond3 == True and cond4 == True:
            cumple = True
        else:
            cumple = False
        aux2 = [cond1, cond2, cond3, cond4, cumple]
        lista2.append(aux2)
    valores = pd.DataFrame(lista, columns = ['Grano','Verdura',
                                             'Fruta','Proteina'])
    valores = pd.concat([archivo['Country'], valores], axis = 1)
    new = pd.DataFrame(lista2, columns = ['Grano', 'Verdura', 'Fruta',
                                          'Proteina', 'Cumple dieta'])
    new = pd.concat([archivo['Country'], new], axis = 1)
    a = archivo['Recovered']
    a = pd.concat([archivo['Country'], a], axis = 1)
    a = a.sort_values(by = 'Recovered', ascending = False)
    a = a.head()
    print("Top 5 paises recuperados analizados. Identificando"
          " si cumplen(True) o no cumplen(False) la Dieta.\n")
    for i in a['Country']:
        print(new[new['Country'] == i])
        print("\n" )
    print("Analizando los valores de consumo de cada"
          " sección (%).\n")
    for i in a['Country']:
        print(valores[valores['Country'] == i])
        print("\n")

#HIPOTESIS D
def chile(archivo):
    lista = []
    grano = archivo.iloc[29, 1] + archivo.iloc[29, 5] + archivo.iloc[29, 13]\
    + archivo.iloc[29, 14] + archivo.iloc[29, 15]
    verd = archivo.iloc[29, 16] + archivo.iloc[29, 18] + archivo.iloc[29, 19]\
    + archivo.iloc[29, 21] + archivo.iloc[29, 22] + archivo.iloc[29, 23]
    frut = archivo.iloc[29, 8] + archivo.iloc[29, 17] + archivo.iloc[29, 20]
    prot = archivo.iloc[29, 2] + archivo.iloc[29,3] + archivo.iloc[29, 4]\
    + archivo.iloc[29, 6] + archivo.iloc[29, 7] + archivo.iloc[29, 9]\
    + archivo.iloc[29, 10] + archivo.iloc[29, 11] + archivo.iloc[29, 12]
    aux = [grano, verd, frut, prot]
    lista.append(aux)
    lista2 = []
    for i in lista:
        cond1 = bool()
        cond2 = bool()
        cond3 = bool()
        cond4 = bool()
        if i[0] > 25 and i[0] < 35:
            cond1 = True
        if i[1] > 35 and i[1] < 45:
            cond2 = True
        if i[2] > 5 and i[2] < 15:
            cond3 = True
        if i[3] > 15 and i[3] < 25:
            cond4 = True
        if cond1 == True and cond2 == True and cond3 == True and cond4 == True:
            cumple = True
        else:
            cumple = False
        aux2 = [cond1, cond2, cond3, cond4, cumple]
        lista2.append(aux2)
    valores = pd.DataFrame(lista, columns = ['Grano','Verdura',
                                             'Fruta','Proteina'])
    new = pd.DataFrame(lista2, columns = ['Grano', 'Verdura', 'Fruta',
                                          'Proteina', 'Cumple dieta'])
    ch = (archivo[archivo['Country'] == 'Chile'])
    print("Chile\n", valores)
    print("\n", new)
    print("\nDemostracion del Cumplimiento(True)"
          " o Inclumplimento(False) de la Dieta dada por USDA.")
    a = archivo[['Country', 'Obesity']]
    b = a.sort_values(by = 'Obesity', ascending = False)
    b = b.head(1)
    chile = a[a['Country'] == 'Chile']
    rec = archivo['Recovered']
    rec = pd.concat([archivo['Country'], rec], axis = 1)
    rec = rec.sort_values(by = 'Recovered', ascending = False)
    rec = rec.head()
    #PROMEDIO DE CHILE EN RECUPERADOS.
    promch = rec.iloc[4,1]
    print(f"\n{rec}")
    print("\nComparacion de los Top5 en Recuperados por COVID.")
    juntar = pd.concat([b.iloc[[0]], chile], axis = 0)
    print(f"\n{juntar}")
    print("\nComparacion del pais con mayor (%) de Obesidad y Chile.\n")
    #PROMEDIO DE TODOS LOS PAISES EN RECUPERADOS.
    aprom = archivo['Recovered'].mean()
    print("\n El promedio de recuperados entre todos los paises"
         f" es: {aprom} Y el promedio de Chile es: {promch}")

#REINICIAR O SALIR
def reinicio():
    qd = input("¿Qué desea hacer?\n"
               "Presione R = Reiniciar\n"
               "Presiona E = Salir.\n"
               "Presione: ")
    if qd.upper() == 'R':
        system('clear')
        menu()
    if qd.upper() == 'E':
        exit()
    else:
        system('clear')
        print("Presione un Boton correcto.")
        reinicio()

#FUNCION  MENU
def menu():
    tecla = input("Presione A: El Top 5 de los paises con mas porcentaje de"
                  " contagio por COVID son, por consecuencia, los paises"
                  " con mayor cantidad de contagio en sus habitantes."
                  " Incluyendo los recuperados, muertos y activos."
                  "\nPresione B: La obesidad y la desnutrición afecta o"
                  " influye en el porcentaje de muerte por COVID."
                  " Ranking de paises Top 10. "
                  "\nPresione C: Los 5 paises con mayor (%)"
                  " de recuperacion no cumplen la dieta requerida. Pero,"
                  " ¿qué caracteristicas de alimentación presentan?"
                  " Datos ordenados por: Granos, verduras, frutas y proteinas."
                  " (Datos establecidos por la Dieta USDA).\n"
                  "Presione D: ¿Chile posee la dieta entregada por USDA?"
                  " Si no es así, ¿permite que obtenga resultados positivos"
                  " en relacion a los recuperados por el contagio del COVID,"
                  " estando por arriba del promedio general de recuperados?"
                  " ¿Que tipo de dieta sigue Chile?\n"
                  "Presione: ")
    system('clear')
    if tecla.upper() == 'A':
        print("El Top 5 de los paises con mas porcentaje de"
              "contagio por COVID son, por consecuencia,"
              " los paises con mayor cantidad de contagio"
              " en sus habitantes, incluyendo los"
              " recuperados, muertos y activos.\n")
        top5_confirmados(archivo)
        print("\n")
        confirmados_pais(archivo)
        print("\n")
        print("Se concluye que el Top 5 de porcentaje es diferente"
              " al de mayor cantidad de habitantes de contagio por COVID."
              " El que mejor representa la tabla de Contagio es"
              " de Habitantes C., ya que cada pais posee diferente"
              " cantidad de poblacion.\n")
        reinicio()
    if tecla.upper() == 'B':
        print("La obesidad y la desnutrición afecta de manera proporcional"
              " en el porcentaje de muerte por COVID"
              " en el Ranking de paises Top 10. Es decir,"
              " entre mas obesidad o desnutrición haya en un país"
              " mayor será el caso de muertos por el virus.\n")
        obesidad_desnutricion(archivo)
        print("\n")
        print("Concluyendo asi que la Obesidad y la Desnutrición  no influye"
              " en el porcentaje de muertes por el virus de"
              " manera proporcional (directa).\n")
        reinicio()
    if tecla.upper() == 'C':
        print("Los 5 paises con mayor (%)"
              " de recuperacion no cumplen la dieta requerida. Pero,"
              " ¿qué caracteristicas de alimentación presentan?"
              " Datos ordenados por: Granos, verduras, frutas y proteinas."
              " (Datos establecidos por la Dieta USDA).\n")
        porcentaje_dieta(archivo)
        print("Se confirma que ningun de los paises con mayor"
              " recuperación cumplen con la dieta entregada por USDA"
              " al establecer un orden de productos por secciones"
              " (granos, verduras, frutas, proteinas)."
              " Concluyendo que en su totalidad"
              " el Top 5 de paises mayormente recuperados"
              " tienen un consumo de alimento caracteristico, es"
              " decir, consumen una gran cantidad de Verduras en un"
              " intervalo entre [50%, 60%], además de un medio"
              " consumo de Proteinas (%) similar al entregado por"
              " la USDA. Infiriendo que su enfoque son estos dos:"
              " Principalmente las verduras y, no menos importantes,"
              " las proteinas. Cabe decir, que el consumo de frutas es"
              " practicamente nulo y los Granos son lejanos al porcentaje"
              " minimo  de consumo entregado por la USDA.\n")
        reinicio()
    if tecla.upper() == 'D':
        print("¿Chile posee la dieta entregada por USDA?"
              " Si no es así, ¿permite que obtenga resultados positivos"
              " en relacion a los recuperados por el contagio del COVID,"
              " estando por arriba del promedio general de recuperados?"
              " ¿Qué tipo de dieta sigue Chile?\n")
        chile(archivo)
        print("\nChile no sigue la dieta entregada por USDA."
              " Sin embargo, su calificación lo hace estar entre"
              " el Top 5 de los paises con mayor porcentaje de"
              " recuperados por COVID, además de estar por arriba del promedio"
              " general de los paises. Por lo tanto, la dieta no es un factor"
              " influyente a la recuperacion. Al ser comparado con el pais"
              " con mayor obesidad, su porcentaje llega a la mitad de el."
              " Es decir, aunque Chile no cumpla la dieta entregada por USDA"
              " no es suficiente para alcanzar un nivel de obesidad tan alto"
              " como los paises con mayor (%) de obesidad. Al"
              " examinar su dieta, refleja un alto consumo de Verduras y"
              " Proteinas, a su vez un bajo nivel de consumo de frutas,"
              " con un porcentaje moderado de ingesta de granos.\n")
        reinicio()
    else:
        print("Por favor introduzcla una tecla correcta.\n")
        menu()

#FUNCION MAIN
def main ():
    menu()


main()
